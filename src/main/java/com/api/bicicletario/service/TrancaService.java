package com.api.bicicletario.service;

import com.api.bicicletario.enumerator.TrancaStatus;
import com.api.bicicletario.model.Totem;
import com.api.bicicletario.model.Tranca;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TrancaService {
    private final Map<Integer, Tranca> trancasMap;
    private Integer nextId;
    @Autowired
    private EmailService emailService;

    @Autowired
    public TrancaService(List<Tranca> trancas) {
        trancasMap = new HashMap<>();
        nextId = 1;

        for (Tranca tranca : trancas) {
            tranca.setId(nextId);
            trancasMap.put(nextId, tranca);
            nextId++;
        }
    }

    public List<Tranca> getTrancas() {
        return new ArrayList<>(trancasMap.values());
    }

    public Tranca getTrancaById(Integer id) {
        return trancasMap.get(id);
    }

    public Tranca createTranca(Tranca tranca) {
        if (trancasMap.containsKey(tranca.getId())) {
            return null;
        }

        int id = nextId++;
        Tranca newTranca = new Tranca(id, tranca.getBicicleta(), tranca.getNumero(), tranca.getLocalizacao(),
                tranca.getAnoDeFabricacao(), tranca.getModelo(), tranca.getStatus());

        trancasMap.put(id, newTranca);
        return newTranca;
    }


    public Tranca updateTranca(Tranca tranca) {
        if (tranca == null) {
            return null;
        }

        if (trancasMap.containsKey(tranca.getId())) {
            trancasMap.put(tranca.getId(), tranca);
            return tranca;
        }

        return null;
    }


    public void deleteTranca(Integer id) {
        emailService.enviarEmail("email@reparador", "retirado com sucesso");

        trancasMap.remove(id);
    }
    public void incluirTrancaEmTotem(Tranca tranca, String emailReparador, Totem totem) {
        if (tranca == null || totem == null) {
            throw new IllegalArgumentException("Os parâmetros não podem ser nulos.");
        }

        // Registra os dados da inclusão da tranca (R1)
        tranca.setStatus(TrancaStatus.LIVRE);


        emailService.enviarEmail("email@reparador", "incluido com sucesso");


        // Realiza o salvamento da tranca no totem (simulado aqui pelo print)
        System.out.println("A tranca foi incluída com sucesso no totem.");
    }


}
